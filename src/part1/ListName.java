package part1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListName {
	private LinkedList<String> list;
	
	public ListName(){
		list = new LinkedList<String>();
	}
	
	public void add(String name){
		list.add(name);
	}
	
	public void remove(String name){
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).equals(name)){
				list.remove(i);
			}
		}
	}
	
	public String check(){
		String str = "";
		for(String s: list){
			str = str + s;
		}
		return str;
	}
}
