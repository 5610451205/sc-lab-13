package part1;

import java.util.Date;

public class GreetingRunnable implements Runnable {
	
	private static final int REPETITION = 10;
	private static final int DELAY = 1000;
	
	private String greeting;
	private ListName act;
	
	public GreetingRunnable(String greeting, ListName act) {
		this.greeting = greeting;
		this.act = act;
		// TODO Auto-generated constructor stub
	}
	@Override
	public void run() {
		try{
			for(int i = 1; i<= REPETITION; i++){
				//Date now = new Date();
				//System.out.println(now + " " + greeting);
				//Thread.sleep(DELAY);
				if(greeting.equals("Hello")){
					act.add("Hello");
				}
				else{
					act.remove("Hello");
				}
				System.out.println(act.check());
				Thread.sleep(DELAY);
			}
			
		}
		catch (InterruptedException exception){
			System.err.println("Error");
		}
		// TODO Auto-generated method stub
		
	}

}
